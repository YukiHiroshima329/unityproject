﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class JointButtonManager : MonoBehaviour
{
    public Text jointText;
    private ARPlaneManager planeManager;

    void Start()
    {
        planeManager = GameObject.Find("AR Session Origin").GetComponent<ARPlaneManager>();
    }

    public void JointButtonClick()
    {
        //３箇所以上計測点がある場合のみJoint処理続行
        if (SetButtonManager.CurrentPositionCount >= 3)
        {
            //Jointボタン押下後フラグをtrueに変更
            SetButtonManager.JointFlag = true;
            jointText.text = "End";

            // 平面検知を停止
            planeManager.enabled = false;
            foreach (var plane in planeManager.trackables)
            {
                if (plane != null)
                {
                    plane.gameObject.SetActive(false);
                }
            }
        }
    }
}
