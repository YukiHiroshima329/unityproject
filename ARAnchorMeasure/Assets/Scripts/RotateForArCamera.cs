﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateForArCamera : MonoBehaviour
{

    // text表示向き設定用ARCamera
    public Camera ARCamera { get; set; }
    
    void Update()
    {
        if (ARCamera != null)
        {
            gameObject.transform.rotation = ARCamera.transform.rotation;
        }

    }
}
