﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

using TMPro;

public class SetButtonManager : MonoBehaviour
{
    [SerializeField] private Camera arCamera;
    [SerializeField] private GameObject ARSessionOrigin;
    [SerializeField] private GameObject LineObject;
    [SerializeField] private GameObject measurePrefab;
    [SerializeField] private Text logText;
    [SerializeField] private Text AnchorLog;
    
    //AR関連
    private ARRaycastManager raycastManager;
    private List<ARRaycastHit> raycastHits = new List<ARRaycastHit>();
    private ARAnchorManager anchorManager;
    private List<ARAnchor> arAnchors = new List<ARAnchor>();
    //リアルタイム距離計測用テキスト
    private GameObject RTMeasureText;
    private TextMeshPro RTTextMesh;
    //計測線関連
    private LineRenderer render;
    //画面中心座標
    private Vector2 centerPosition;
    //初回計測点座標
    private Vector3 firstPosition;
    //前回計測点座標
    private Vector3 beforePosition;
    //現在の計測点カウント
    public static int CurrentPositionCount = 0;
    //Jointボタン押下フラグ
    public static bool JointFlag = false;
    //処理終了フラグ
    private bool EndFlag = false;
    
    void Start()
    {
        //コンポーネント取得
        raycastManager = ARSessionOrigin.GetComponent<ARRaycastManager>();
        anchorManager = ARSessionOrigin.GetComponent<ARAnchorManager>();
        //画面中心座標取得
        centerPosition = new Vector2(Screen.width / 2, Screen.height / 2);
        //LineRendererコンポーネント取得＋線の形を設定
        render = LineObject.GetComponent<LineRenderer>();
        render.numCapVertices = 10;
        render.numCornerVertices = 10;
    }

    public void SetButtonClick()
    {
        //Jointボタン押下後は処理しない
        if (JointFlag) return;
        // 平面検知出来なければ処理終了
        if (!raycastManager.Raycast(centerPosition, raycastHits, TrackableType.PlaneWithinPolygon)) return;

        //RayがHitした平面座標取得
        Vector3 placePosition = raycastHits[0].pose.position;
        //ARAnchor追加処理
        arAnchors.Add(anchorManager.AddAnchor(raycastHits[0].pose));
        //アンカーの親にARSessionOriginを設定
        arAnchors[arAnchors.Count - 1].transform.parent = ARSessionOrigin.transform;
        
        //初回処理
        if (arAnchors.Count <= 1)
        {
            //初回座標を記録（Jointボタン押下時、最後の座標と線を繋げるため）
            firstPosition = placePosition;
            //動的に次のアンカー予定座標の距離を表示させるテキストを生成
            RTMeasureText = Instantiate(measurePrefab, placePosition, Quaternion.identity);
            //text表示をカメラに向かせるためARCameraを設定する
            RTMeasureText.GetComponent<RotateForArCamera>().ARCamera = arCamera;
            //テキストコンポーネントを取得(リアルタイム距離計測処理で使用)
            RTTextMesh = RTMeasureText.GetComponent<TextMeshPro>();
        }
        else
        {
            //２回目以降の処理
            //２点間の中間座標を求める
            Vector3 textPosition = ((placePosition - beforePosition) / 2) + beforePosition;
            //２点間の距離を表示するテキストを生成
            var measureText = Instantiate(measurePrefab, textPosition, Quaternion.identity);
            var textMesh = measureText.GetComponent<TextMeshPro>();
            textMesh.text = MeasureText(placePosition);
            //text表示をカメラに向かせるためARCameraを設定する
            measureText.GetComponent<RotateForArCamera>().ARCamera = arCamera;
            //画面上に計測距離のログを表示させる
            logText.text += MeasureText(placePosition) + "\n";
        }
        
        //LineRendererのPositionsのサイズを増やす（リアルタイム距離計測用に最大値を増やしておく）
        render.positionCount = CurrentPositionCount + 2;
        //LineRendererのPositionsに現在のRayの位置情報を追加
        render.SetPosition(CurrentPositionCount, placePosition);
        //現在の計測点のカウントを加算
        CurrentPositionCount++;
        //今回の座標を記録（次回の距離計測用）
        beforePosition = placePosition;
    }

    void Update()
    {
        //アンカーが一個以上存在する場合のみ処理開始
        if (arAnchors.Count >= 1)
        {
            //リアルタイムアンカー座標ログ表示処理
            AnchorLog.text = "";
            // AnchorLog.text += "アンカー１セッションID\n" + arAnchors[0].sessionId + "\n";
            AnchorLog.text += "アンカー１座標x = " + string.Format("{0:0.0000000000}", arAnchors[0].transform.position.x) + "\n";
            AnchorLog.text += "アンカー１座標y = " + string.Format("{0:0.0000000000}", arAnchors[0].transform.position.y) + "\n";
            AnchorLog.text += "アンカー１座標z = " + string.Format("{0:0.0000000000}", arAnchors[0].transform.position.z) + "\n";
            if (CurrentPositionCount >= 2)
            {
                // AnchorLog.text += "アンカー２セッションID\n" + arAnchors[1].sessionId + "\n";
                AnchorLog.text += "アンカー２座標x = " + string.Format("{0:0.0000000000}", arAnchors[1].transform.position.x) + "\n";
                AnchorLog.text += "アンカー２座標y = " + string.Format("{0:0.0000000000}", arAnchors[1].transform.position.y) + "\n";
                AnchorLog.text += "アンカー２座標z = " + string.Format("{0:0.0000000000}", arAnchors[1].transform.position.z) + "\n";
            }

            //アンカー座標にLineRendererのPositionsを更新し続ける処理（アンカーから線の頂点が外れる誤差が修正される）
            for (int i = 0; i < CurrentPositionCount; i++)
            {
                render.SetPosition(i, arAnchors[i].transform.position);

                //Jointボタン押下後かつループ最後の場合、最後の計測点を最初のアンカー座標に繋げる
                if (JointFlag && i == CurrentPositionCount - 1)
                {
                    render.SetPosition(i + 1, arAnchors[0].transform.position);
                }
            }

            //最終計測処理終了後、次回Updateはここで処理終了
            if (EndFlag) return;

            //次の計測点までの距離をリアルタイムで表示する処理
            //平面検知出来なければ処理終了
            if (!raycastManager.Raycast(centerPosition, raycastHits, TrackableType.PlaneWithinPolygon)) return;
            //RayがHitした平面座標を元に距離計測＆表示
            RTMeasureTextUpdate(raycastHits[0].pose.position);

            //Jointボタン押下時、最終計測処理
            if (JointFlag)
            {
                //最初に設置したアンカーの位置に最後の線を繋げる
                RTMeasureTextUpdate(firstPosition);
                //画面上にログを表示させる
                logText.text += MeasureText(firstPosition);
                //終了フラグをtrueにして以降のリアルタイム距離計測を無効化
                EndFlag = true;
            }
        }
    }

    /// <summary>
    /// リアルタイム距離計測処理
    /// </summary>
    /// <param name="nextPosition">距離を計測したい次座標</param>
    void RTMeasureTextUpdate(Vector3 nextPosition)
    {
        //LineRendererの最新Positionsを引数の座標に更新
        render.SetPosition(CurrentPositionCount, nextPosition);
        //２点間の中間座標を求める
        Vector3 textPosition = ((nextPosition - beforePosition) / 2) + beforePosition;
        //動的メジャーの座標を更新
        RTMeasureText.transform.position = textPosition;
        //動的メジャーの距離表示を更新
        RTTextMesh.text = MeasureText(nextPosition);
    }

    /// <summary>
    /// ２点間の距離と番号を特定の書式で返す処理
    /// </summary>
    /// <param name="nextPosition">距離を計測したい次座標</param>
    /// <returns></returns>
    String MeasureText(Vector3 nextPosition)
    {
        //今回と前回の２点間の距離を求める
        float dist = Vector3.Distance(nextPosition, beforePosition);
        //動的メジャーまたはログの表示用に書式を整えて返す
        return "No: " + CurrentPositionCount + "\n" + 
            Math.Round(dist, 2, MidpointRounding.AwayFromZero) + "(m)";
    }
    
    
}
