﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeButtonClick : MonoBehaviour
{
    public Text buttonText;
    public GameObject A;
    public GameObject B;

    // Start is called before the first frame update
    void Start()
    {
        buttonText.text = "Cube";
        ARPlacementManager.placementPrefab = A;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick(){
        if (FlagManager.unitychanMode)
        {
            FlagManager.unitychanMode = false;
            buttonText.text = "Cube";
            ARPlacementManager.placementPrefab = A;
        }else{
            FlagManager.unitychanMode = true;
            buttonText.text = "unitychan";
            ARPlacementManager.placementPrefab = B;
        }
    }
}
