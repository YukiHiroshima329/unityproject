﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonClick : MonoBehaviour
{
    public Text buttonText;
 
    // Start is called before the first frame update
    void Start()
    {
        buttonText.text = "移動モード";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick(){
        if (FlagManager.DeleteMode)
        {
            FlagManager.DeleteMode = false;
            buttonText.text = "移動モード";
        }else{
            FlagManager.DeleteMode = true;
            buttonText.text = "削除モード";
        }
        
    }
}
