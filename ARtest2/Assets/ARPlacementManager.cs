﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARPlacementManager : MonoBehaviour
{
    [SerializeField] public static GameObject placementPrefab;
    [SerializeField] private Camera arCamera;
    [SerializeField] private ARRaycastManager raycastManager;    
    private List<ARRaycastHit> raycastHits = new List<ARRaycastHit>();
    
    private GameObject SelectedObject;
    private Vector3 currentposition = new Vector3(0.0f, 0.0f, 0.0f);

    private void Update()
    {
        //画面に触れている箇所の数が０なら処理終了
        if (Input.touchCount <= 0) return;

        //最初にタップした箇所を取得
        var touch = Input.GetTouch(0);
        // arCameraカメラからrayを照射
        var ray = arCamera.ScreenPointToRay(touch.position);

        //画面に触れた瞬間の処理
        if (touch.phase == TouchPhase.Began)
        {
            Debug.Log("touchBegan開始");

            RaycastHit hit;
            // オブジェクトに反応するRayを使用してヒットした場合
            if (Physics.Raycast(ray, out hit))
            {
                Debug.Log("オブジェクトにPhysics.RaycastがHit！");

                //Rayがヒットしたオブジェクトの名前が「Cube」ならインスタンスに格納
                if (hit.collider.gameObject.name.Contains("Cube"))
                {
                    Debug.Log("これはCubeです");
                    SelectedObject = hit.collider.gameObject;
                    //削除モードがオンの場合オブジェクトを削除。
                    if (FlagManager.DeleteMode)
                    {
                        Destroy(SelectedObject);
                    }
                    return;
                }

                //Rayがヒットしたオブジェクトの名前が「unitychan」ならインスタンスに格納
                if (hit.collider.gameObject.name.Contains("unitychan"))
                {
                    Debug.Log("これはunitychanです");
                    SelectedObject = hit.collider.gameObject;
                    //削除モードがオンの場合オブジェクトを削除。
                    if (FlagManager.DeleteMode)
                    {
                        Destroy(SelectedObject);
                    }
                    return;
                }
            }

            // 平面検知した場所に反応するRayを使用
            if (raycastManager.Raycast(ray, raycastHits, TrackableType.PlaneWithinPolygon))
            {
                // 複数のPlaneがあった場合、最も近いPlaneが0番目に入っている
                Pose pose = raycastHits[0].pose;
                // 配置すべき座標
                Vector3 placePosition = pose.position;
                //設定されたプレハブを画面に配置
                // Instantiate(placementPrefab, placePosition, Quaternion.identity);
                SelectedObject = Instantiate(placementPrefab, placePosition, Quaternion.Euler(0, 180, 0));
                currentposition = SelectedObject.transform.position;
                Debug.Log("「" + placementPrefab.name + "」を召喚！");
            }
        }
        
        //画面上で指が動いた時の処理
        if (touch.phase == TouchPhase.Moved)
        {
            Debug.Log("touchMoved中");

            // 平面検知した場所に反応するRayを使用
            if (raycastManager.Raycast(ray, raycastHits, TrackableType.PlaneWithinPolygon))
            {
                Debug.Log("移動中");
                // 複数のPlaneがあった場合、最も近いPlaneが0番目に入っている
                Pose pose = raycastHits[0].pose;
                // 配置すべき座標
                Vector3 placePosition = pose.position;
                //フレーム毎に選択中のオブジェクトの位置を変更し続ける
                SelectedObject.transform.position = placePosition;
            }
        }
        
        if (currentposition != SelectedObject.transform.position)
        {
            Debug.Log("座標変更有！");
            Debug.Log("Log:配置オブジェクトID = " + SelectedObject.GetInstanceID());
            Debug.Log("Log:配置オブジェクト座標x = " + string.Format("{0:0.0000000000}", SelectedObject.transform.position.x));
            Debug.Log("Log:配置オブジェクト座標y = " + string.Format("{0:0.0000000000}", SelectedObject.transform.position.y));
            Debug.Log("Log:配置オブジェクト座標z = " + string.Format("{0:0.0000000000}", SelectedObject.transform.position.z));
            currentposition = SelectedObject.transform.position;
        }
    }
    
}