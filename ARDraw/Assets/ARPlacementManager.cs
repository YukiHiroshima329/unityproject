﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARPlacementManager : MonoBehaviour
{
    [SerializeField] private GameObject placementPrefabA;
    [SerializeField] private GameObject placementPrefabB;
    [SerializeField] private Camera arCamera;
    [SerializeField] private ARRaycastManager raycastManager;
    public static bool Flag = true;
    private List<ARRaycastHit> raycastHits = new List<ARRaycastHit>();
    
    //現在描画中のLineObject;
    private GameObject CurrentLineObject = null;

    private void Update()
    {
        //画面に触れている箇所の数が０なら処理終了
        if (Input.touchCount <= 0) return;

        //最初にタップした箇所を取得
        var touch = Input.GetTouch(0);
        // arCameraカメラからrayを照射
        var ray = arCamera.ScreenPointToRay(touch.position);

        //画面に触れた瞬間の処理
        if (touch.phase == TouchPhase.Moved)
        {
            RaycastHit hit;
            // オブジェクトに反応するRayを使用してヒットした場合
            if (Physics.Raycast(ray, out hit))
            {
                Debug.Log("オブジェクトにPhysics.RaycastがHit！");

                //Rayがヒットしたオブジェクトの名前が「CanvasTrigger」ならインスタンスに格納
                if (hit.collider.gameObject.name.Contains("CanvasTrigger"))
                {
                    Debug.Log("これはCanvasTriggerです");

                    if (Flag)
                    {
                        if(CurrentLineObject == null)
                        {
                            //PrefabからLineObjectを生成
                            CurrentLineObject = Instantiate(placementPrefabA, new Vector3(0, 0, 0), Quaternion.identity);
                            return;
                        }
                        Debug.Log("これは" + CurrentLineObject + "です。");

                        //ゲームオブジェクトからLineRendererコンポーネントを取得
                        LineRenderer render = CurrentLineObject.GetComponent<LineRenderer>();

                        //LineRendererからPositionsのサイズを取得
                        int NextPositionIndex = render.positionCount;

                        //LineRendererのPositionsのサイズを増やす
                        render.positionCount = NextPositionIndex + 1;

                        //LineRendererのPositionsに現在のコントローラーの位置情報を追加
                        render.SetPosition(NextPositionIndex, hit.point);

                    }else{
                        //玉召喚疑似お絵描き処理
                        Instantiate(placementPrefabB, hit.point, Quaternion.identity);
                    }
                }
            }
        }

        //画面に触れ終わった瞬間の処理
        if (touch.phase == TouchPhase.Ended){
            if(CurrentLineObject != null)
            {
                //現在描画中の線があったらnullにして次の線を描けるようにする。
                CurrentLineObject = null;
            }
        }
    }
}