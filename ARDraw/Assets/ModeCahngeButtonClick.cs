﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModeCahngeButtonClick : MonoBehaviour
{
    public Text buttonText;

    // Start is called before the first frame update
    void Start()
    {
        buttonText.text = "完成版";
    }

    public void OnClick(){
        if (ARPlacementManager.Flag)
        {
            ARPlacementManager.Flag = false;
            buttonText.text = "残念版";
        }else{
            ARPlacementManager.Flag = true;
            buttonText.text = "完成版";
        }
    }
}
